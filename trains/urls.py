from django.urls import path

from . import views

urlpatterns = [
    path('train_route_data', views.train_route_data, name='train_route_data'),
    path('route_best_train',views.route_best_train, name='route_best_train'),
    path('tracks_all',views.tracks_all, name="tracks_all"),
    path('locomotive_class', views.locomotive_class, name = "locomotive_class"),
    path('locomotive_detail', views.locomotive_detail, name="locomotive_detail")
]