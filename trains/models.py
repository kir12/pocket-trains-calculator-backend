from django.db import models

# Create your models here.

class City(models.Model):
    class ContinentChoices(models.TextChoices):
        NORTH_AMERICA = 'NA',"North America" 
        SOUTH_AMERICA = 'SA', "South America"
        EUROPE = "EU","Europe"
        ASIA = "AS","Asia"
        AFRICA = "AF", "Africa"
        OCEANIA = "OC", "Oceania"

    name = models.CharField(max_length=50)
    continent = models.CharField(max_length=2,choices=ContinentChoices.choices,default=ContinentChoices.EUROPE)

    def __str__(self):
        return self.name 
class Track(models.Model):
    left_city = models.ForeignKey(City, on_delete = models.CASCADE, related_name="left_city")
    right_city = models.ForeignKey(City, on_delete = models.CASCADE, related_name="right_city")
    fuel_cost = models.IntegerField(default=0)
    coin_cost = models.IntegerField(default=0)
    is_water = models.BooleanField(default=False)

    def between_continents(self):
        return self.left_city.continent != self.right_city.continent
    
    # coin values listed on pocket trains wiki, therefore used as determination of miles
    # NOTE: obsolete following Enson's note of calculating 
    def get_distance_miles(self):
        #cost_per_mile = 50 if not self.is_water else 150
        #return self.coin_cost/cost_per_mile

        #new rule from enson
        return self.fuel_cost/15
    
    def get_resell_price(self):
        return self.coin_cost * 0.1
    
    def __str__(self):
        return str(self.left_city) + " - " + str(self.right_city)

class TrainManager(models.Manager):
    def max_fuel(self):
        return self.base_fuel * (self.max_fuel_cars+1)

class Train(models.Model):
    class TrainModelChoices(models.TextChoices):
        STEAMER = 'ST',"Steamer"
        EXPRESS = 'EX',"Express"
        STANDARD = 'STD', "Standard"
        ZEPHYR = 'ZE', "Zephyr"
        FREIGHTER = 'FR', "Freighter"
        HYBRID = "HY", "Hybrid"
        SPECIAL = "SP", "Special"

    @staticmethod
    def trainmodelchoice_reverse(locomotive_class):
        return {
            "steamer":"ST",
            "express":"EX",
            "standard":"STD",
            "zephyr":"ZE",
            "hreighter":"FR",
            "hybrid":"HY",
            "special":"SP"
        }.get(locomotive_class, None)


    name = models.CharField(max_length=50)
    classification = models.CharField(max_length=3, choices=TrainModelChoices.choices, default = TrainModelChoices.STEAMER)
    speed = models.IntegerField()
    base_cars = models.IntegerField()
    base_fuel = models.IntegerField()
    max_engines = models.IntegerField()
    max_fuel_cars = models.IntegerField()
    objects = TrainManager()

    def __str__(self):
        return self.name

    def get_max_configurations(self):
        return {
            "max_fuel":self.base_fuel * (self.max_fuel_cars+1),
            "max_cars":self.base_cars + ((self.max_engines-1) * self.base_cars/2)
        }
    