from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from trains.models import *

import os
import csv

class Command(BaseCommand):
    help = 'Initializes Pocket Trains data'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        # delete old data from database
        City.objects.all().delete()
        Train.objects.all().delete()
        Track.objects.all().delete()

        data_dir=os.path.join(os.path.dirname(__file__),"train_data")

        continent_to_choice = {
            "africa":"AF",
            "asia":"AS",
            "europe":"EU",
            "north_america":"NA",
            "oceania":"OC",
            "south_america":"SA",
        }

        #import city data
        for continent_file in os.listdir(os.path.join(data_dir,"cities")):
            f = open(os.path.join(data_dir,"cities",continent_file))
            #add each city in continent to db
            for line in f:
                City.objects.create(name=line.strip("\n"), continent=continent_to_choice[os.path.splitext(continent_file)[0]])

        #import track data
        with open(os.path.join(data_dir,"track_file.csv"),"r") as track_file:
            reader = csv.reader(track_file)
            next(reader, None)  # skip the headers
            for row in reader:
                aleft_city = City.objects.filter(name__contains=row[0].replace("_"," "))[0]
                aright_city = City.objects.filter(name__contains=row[1].replace("_"," "))[0]
                #TODO: consider adding data for scrap and sell

                if len(Track.objects.filter(Q(left_city=aleft_city, right_city= aright_city) | Q(left_city= aright_city, right_city= aleft_city))) == 0:
                    Track.objects.create(left_city =aleft_city, right_city=aright_city, fuel_cost = int(row[3]), coin_cost = int(row[4]), is_water = row[2] == "Y")

        # import train data
        with open(os.path.join(data_dir, "train__data.csv"), "r") as train_file:
            reader = csv.reader(train_file)
            next(reader,None)
            for row in reader:
                # determine class of train here
                if row[10] == "N/A" or int(row[10])==1:
                    train_class = "SP"
                else:
                    itemized_train = row[0].split(" ")
                    train_class = Train.trainmodelchoice_reverse(itemized_train[-1])
                Train.objects.create(name=row[0], classification=train_class, speed = int(row[1]), base_cars = int(row[2]), base_fuel = int(row[4]), max_engines=int(row[6]), max_fuel_cars=int(row[7]))
                        