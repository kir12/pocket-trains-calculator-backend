from rest_framework import serializers
from .models import *

class TrackSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source="__str__")
    class Meta:
        model = Track
        fields = ["full_name","fuel_cost","coin_cost","is_water"]

class TrainSerializer(serializers.ModelSerializer):
    class Meta:
        model = Train
        fields="__all__"
