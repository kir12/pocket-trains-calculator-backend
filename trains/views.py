from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Q, Avg, F
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.decorators import api_view, renderer_classes
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
from rest_framework.renderers import JSONRenderer

from .models import *
from .serializers import *

import math
import pdb
import sys

# HELPER FUNCTION, not actual view!
# given a string of cities separated by |, returns array of tracks and total accumulated fuel cost
def parse_cities(citys):

    # validation of cities
    if len(citys)<=1:
            raise NotFound("Your railroad must have at least two stops.")

    fuel_used = 0
    track_list = []

    # iterate from start to one before last city
    for i in range(len(citys)-1):

        # get a pair of cities and determine if they're valid cities
        city_objects = [None]*2
        for a in range(2):
            city_objects[a] = City.objects.filter(Q(pk__iexact=citys[i+a]) | Q(name__contains=citys[i+a]))
            if len(city_objects[a]) != 1: raise NotFound("The city " + citys[i+a] + " was not found.")
            city_objects[a] = city_objects[a][0]

        # attempt to find a track between them
        track = Track.objects.filter(Q(left_city=city_objects[0], right_city= city_objects[1]) | Q(left_city= city_objects[1], right_city= city_objects[0]))
        if len(track) != 1: 
            raise NotFound("There is no track connecting " + city_objects[0].name + " and " + city_objects[1].name)
        track=track[0]

        fuel_used += track.fuel_cost
        track_list.append(track)
    return fuel_used,track_list 

# HELPER FUNCTION
# given a train and fuel elapsed, computes the required time for that trip to occur
def elapsed_time(train_speed, fuel_used):
    return fuel_used * 4 / train_speed

# given a track, determines the optimal locomotive (plus fuel configuration) to go that ride (or multiple locomotives)
# goals: to balance out between number of stops visitable (fuel limiation) and playtime required to accomodate said stops (speed)
# required parameters: selected track
# optional parameters: faster, slower, or close to average time (or ignoring all time constants)
# NOTE to include to user: best results with maxed-out trains and fuelsets
@api_view(["GET"])
def route_best_train(request):

    # validate cities parameter
    if "cities" not in request.query_params:
        raise NotFound("Cities not specified")
    cities_raw = request.query_params.get("cities").split("|")

    # get validated list of cities and fuel cost
    fuel_cost ,track_list = parse_cities(cities_raw)

    # eliminate the train it cannot make a minimum of two trips.
    # reason: two trips is base minimum for viability
    # original expression: base_fuel * (max_fuel+1) / fuel_cost > 2
    # NOTE: order_by gives baseline ordering, but finalized oredering not until later
    locomotive_list = Train.objects.filter(base_fuel__gt=2*fuel_cost/(F('max_fuel_cars')+1)).order_by("-base_cars", "-max_engines")
    
    #compute average elapsed time for single trip based on average speed from valid locomotives
    avg_elapsed_time = elapsed_time(locomotive_list.aggregate(Avg('speed'))['speed__avg'],fuel_cost)

    # attempt to get time factor, and default to None if no valid options entered
    time_param = {
        'slow':0,
        'fast':1,
        'average':2,
    }.get(request.query_params.get("time"))

    def time_functor(train):

        # gets difference of elapsed time between specific train and average of all valid trains
        time_diff = elapsed_time(train.speed,fuel_cost) - avg_elapsed_time

        # eliminate the train if it doesn't fall under the desired time factor
        if time_param == 0:
            time_test = time_diff > 0
        elif time_param == 1:
            time_test = time_diff < 0
        elif time_param == 2:
            time_test = abs(time_diff) < 2
        else:
            time_test = True
        return time_test

    # remove any locomotives that don't fall under the time parameters
    locomotive_list = [train for train in locomotive_list if time_functor(train)]

    # do so by calculating number of possible jobs that can be fulfilled
    # this way, fuel cost, speed, and train capacity all play some kind of factor
    def calc_num_jobs(train):
        # all trains guaranteed 2 trips minimum
        single_trip_jobs=train.get_max_configurations()["max_cars"] * (len(track_list) + 1)
        output =  single_trip_jobs * 2
        train_fuel_left = train.get_max_configurations()["max_fuel"] - (fuel_cost * 2)

        # while train can still do whole trips, skip track calculation
        back_to_beginning=True
        while train_fuel_left > fuel_cost:
            output += single_trip_jobs
            train_fuel_left -= fuel_cost
            back_to_beginning = not back_to_beginning

        # now "simulate" train going from track to track to get exact point of train failure
        for track in track_list if back_to_beginning else reversed(track_list):
            # stop if train can't go further, otherwise proceed
            if track.fuel_cost > train_fuel_left:
                return output
            output += train.get_max_configurations()["max_cars"]
            train_fuel_left -= track.fuel_cost

        return output

    # generate total possible jobs for each locomotive, and then sort
    job_counts = [calc_num_jobs(train) for train in locomotive_list]
    
    # sort using Python's sort function
    locomotive_list.sort(key = dict(zip(locomotive_list,job_counts)).get, reverse=True)

    # return list of trains with train serializer from django rest framework
    return Response(TrainSerializer(locomotive_list, many=True).data)

# Given a locomotive, fuel selection, and route, determines if it can successfully go the entire way in one swoop
# also determines how many complete trips are possible, and the time minimum required for a single trip
@api_view(["GET"])
def train_route_data(request):

    #check if any paramters not supplied
    if any(param not in request.query_params for param in ["locomotive","num_fuel_cars","cities"]):
        raise NotFound("One of the required parameters was not supplied.")

    # process train
    train_obj = Train.objects.filter(Q(pk__iexact=request.query_params.get('locomotive')) | Q(name__contains=request.query_params.get('locomotive')))
    if len(train_obj)!=1: raise NotFound("The train you entered was not found. Please refine your query.")
    train_obj=train_obj[0]

    citys = request.query_params.get('cities').split('|')

    # valiation of fuel cars
    try: 
        num_fuel_cars = int(request.query_params.get('num_fuel_cars'))
    except TypeError: #handle fuel cars
        raise NotFound("The number of fuel cars is malformed, please try again.")

    #more fuel cars than supported by train 
    if num_fuel_cars > train_obj.max_fuel_cars: 
        raise NotFound("You specified more fuel cars than the " + train_obj.name + " can accomodate. Please try again.")

    #compute fuel setup of player's specific setup
    fuel_configuration = train_obj.base_fuel * (num_fuel_cars+1)

    fuel_used,track_list = parse_cities(citys)

    #compute coin cost
    cost_breakdown = {}
    total_coin_cost = 0

    for track in track_list:
        track_specific_dict = {}
        track_specific_dict["base_cost"] = track.coin_cost
        track_specific_dict["intercontinental_cost"] = 50000 if track.between_continents() else 0
        track_specific_dict["water_tax"] = track.is_water
        cost_breakdown[track.__str__()] = track_specific_dict
        total_coin_cost+= track_specific_dict["base_cost"] + track_specific_dict["intercontinental_cost"]

    # write all data to output JSON
    total_trips = math.floor(fuel_configuration/fuel_used)
    output_dict={}
    output_dict["single_trip_possible"] =total_trips>0
    output_dict["total_num_trips"]=total_trips
    output_dict["total_coin_cost"]=total_coin_cost
    output_dict["cost_breakdown"] = cost_breakdown

    elapsed_time = fuel_used * 4 / train_obj.speed # follows new rule from enson
    if(total_trips>0):
        output_dict["single_trip_time_min"] = math.floor(elapsed_time) if elapsed_time >= 1 else "< 1"
    return Response(output_dict)

@api_view(["GET"])
def tracks_all(request):
    track_serializer = TrackSerializer(Track.objects.all(), many=True)
    return Response(track_serializer.data)

@api_view(["GET"])
@renderer_classes([JSONRenderer])
def locomotive_class(request):
    output = {}
    for item in Train.TrainModelChoices.choices:
        output[item[0]] = item[1]
    return Response(output)

@api_view(["GET"])
def locomotive_detail(request):
    output = Train.objects.all()
    if "class" in request.query_params:
        output = output.filter(classification=Train.trainmodelchoice_reverse(request.query_params.get("class").lower()))
    if "name" in request.query_params:
        output = output.filter(name=request.query_params.get("name"))
    return Response(TrainSerializer(output, many=True).data)