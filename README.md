# pocket-trains-calculator-backend

This is the backend for pocket-trains-calculator, an API/database for all things related to the iOS/Android app "Pocket Trains." 
The backend itself is usable as a standard REST API, but is paired with `pocket-trains-calculator-frontend` on the website as a full web application.
It has two main functions:

1. A central database to pull general information about locomotives, tracks, and cities

2. A utility for determining the cost/viablity of track routes, and determining good locomotives for desired track routes

All track/locomotive data was derived from the [Pocket Trains Wiki](https://pocket-trains.fandom.com/wiki/Pocket_Trains_Wiki).
Please head there for more information about Pocket Trains.

## Tech Stack & API Endpoints

The backend uses SQLite and Django Rest Framework. 
(PostgreSQL is a bit too overkill for a smal app like this one)
Details regarding deploying to production have not been established yet since frontend work (and API work) is not complete.

The following API Endpoints are accessable, or are planned:

### `/route_best_train`

Determines which locomotives are most optimal for your desired path, taking into consideration locomotive speed, fuel capacity, and car capacity.
*NOTE: all calculations are made with maxed out locomotives and fuel cars in mind.*

- Required Parameters: 
  - `cities`: A list of cities separated by `|`  characters (e.g. `cities=New York|Lisbon|Madrid`) 

    Each track should be connected **directly** to the next one. (e.g. In the example, `New York` should have a direct track connection to `Lisbon`) Locomotives that cannot make a minimum of two trips of your selected track are not considered.

- Optional Parameters:
  - `time`: One of the following options: `slow`, `fast`, or `average`. 
  
    Selecting this option refines the locomotive list down to those whose elapsed trip time match the parameter. (e.g. Selecting `fast` yields locomotives which run faster than the average elapsed time) 
  
    If this option is omitted, all valid locomotives are considered.

### `/train_route_data`

Determines if a locomotive can complete a selected track route in one sitting without refueling midway. Also computes the total required cost and cost breakdown of creating the track route, including any required license purchasing.

- Required Paramters:
  - `locomotive`: A valid locomotive or ID of a locomotive.
  - `num_fuel_cars`: Selected number of fuel cars, up to the locomotive's supported max.
  - `cities`: A list of cities separated by `|`  characters (e.g. `cities=New York|Lisbon|Madrid`) 

    Each track should be connected **directly** to the next one. (e.g. In the example, `New York` should have a direct track connection to `Lisbon`) 

- Optional Parameters:
  - None

### `/tracks_all`

Returns information on all tracks.

- No Parameters

### `/locomotive_class`

Returns a list of all the types of locomotives.

- No Parameters


### `/locomotive_detail`

Returns information on locomotives.

- Required Parameters:
  - None

    If no parameters are entered, then information on all locomotives will be returned.

- Optional Paramters:
  - `class`: A pre-selected locomotive class. 

    If this option is selected, all locomotives that fall under the selected class will be returned.

  - `name`: A pre-selected locomotive.

    If this option is selected, information specific to this locomotive will be returned.

**The below API endpoints have not been implemented yet.**

## Requirements

- Python 3.x
- SQLite
- Linux, Mac OS, WSL, or any other UNIX terminal
- Git

## Installation

1. Clone the repo: Run `git clone https://gitlab.com/kir12/pocket-trains-calculator-backend.git` at any path and then `cd /path/to/pocket-trains-calculator-backend/`
2. Create a Python virtual environment: Run `python -m venv [ENV_NAME]` (or whatever command you usually use) and then activate it with `source [ENV_NAME]/bin/activate`
3. Install Python Dependencies: Run `pip install -r requirements.txt`
4. Create database table structure: Run `python manage.py migrate`
5. Fill database with Pocket Trains data: Run `python manage.py init_data`
6. Start the development serer: Run `python manage.py runserver`

## Contribution

If there any errors in train/track data, (or any bugs) please let me know via the [issue page](https://gitlab.com/kir12/pocket-trains-calculator-backend/-/issues). If you have a desired code change, please submit a [merge request](https://gitlab.com/kir12/pocket-trains-calculator-backend/-/merge_requests).